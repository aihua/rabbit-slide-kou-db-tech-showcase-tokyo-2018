= MySQL・PostgreSQLだけで作る高速あいまい全文検索システム

ナイーブな全文検索は単なる中間一致検索です。しかし、現実に人が入力するテキストは揺れまくっているので単なる中間一致検索では必要な情報を見つけられません。たとえば、「(090)1234-5678」も「０９０ー１２３４ー５６７８」も「09012345678」もすべて同じ電話番号として扱いたいのが現実です。

MySQLとPostgreSQLの全文検索機能ではこのようにゆるい全文検索を高速に実現することができません。そこでMroonga・PGroongaです。Mroonga・PGroongaを使ってMySQL・PostgreSQLだけで高速にゆるく全文検索する方法を紹介します。

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide-kou-db-tech-showcase-tokyo-2018

=== 表示

  rabbit rabbit-slide-kou-db-tech-showcase-tokyo-2018.gem

